package com.irving.ingresso.model.map;

import com.irving.ingresso.model.Usuario;
import io.openapiprocessor.generated.model.UsuarioDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UsuarioMapper {
    UsuarioDTO usuarioToUsuarioDTO(Usuario usuario);
    Usuario usuarioDTOtoUsuario(UsuarioDTO usuarioDTO);
}
