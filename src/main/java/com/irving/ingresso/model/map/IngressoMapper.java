package com.irving.ingresso.model.map;

import com.irving.ingresso.model.Ingresso;
import io.openapiprocessor.generated.model.IngressoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface IngressoMapper {
    IngressoDTO ingressoToIngressoDTO(Ingresso ingresso);
    Ingresso ingressoDTOtoIngresso(IngressoDTO ingressoDTO);
}
