package com.irving.ingresso.model.map;

import com.irving.ingresso.model.Evento;
import io.openapiprocessor.generated.model.EventoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EventoMapper {

    EventoDTO eventoToEventoDTO(Evento evento);
    Evento eventoDTOtoEvento(EventoDTO eventoDTO);

}
