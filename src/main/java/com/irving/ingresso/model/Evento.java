package com.irving.ingresso.model;

import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table( name = "Evento", schema = "public")
@Getter @Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Evento {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column( name = "id" )
    private Long id;
    @Column( name = "titulo" )
    private String titulo;
    @Column( name = "datahora" )
    private OffsetDateTime datahora;
    @Column( name = "preco" )
    private Double preco;


}
