package com.irving.ingresso.controller;

import com.irving.ingresso.model.Evento;
import com.irving.ingresso.model.map.EventoMapper;
import com.irving.ingresso.repository.EventoRepository;
import io.openapiprocessor.generated.api.EventoApi;
import io.openapiprocessor.generated.model.EventoDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequiredArgsConstructor
public class EventoController implements EventoApi {
    private final EventoRepository eventoRepository;
    private final EventoMapper eventoMapper;

    @Override
    public EventoDTO listaEvento(Long id) {
        Evento evento = eventoRepository.findById(id).get();
        return eventoMapper.eventoToEventoDTO(evento);
    }

    @Override
    public void atualizaEvento(Long id, EventoDTO body) {
        if (id == body.getId() && eventoRepository.findById(id).isPresent()) {
            eventoRepository.save(eventoMapper.eventoDTOtoEvento(body));
        }
    }

    @Override
    public void deletaEvento(Long id) {
        eventoRepository.deleteById(id);
    }

    @Override
    public EventoDTO[] listaEventos() {
        ArrayList<EventoDTO> eventosDTOList = new ArrayList<EventoDTO>();
        eventoRepository.findAll().forEach(evento -> {
            eventosDTOList.add(eventoMapper.eventoToEventoDTO(evento));
        });
        EventoDTO[] toReturn = new EventoDTO[eventosDTOList.size()];

        return eventosDTOList.toArray(toReturn);
    }

    @Override
    public void adicionaEvento(EventoDTO body) {
        eventoRepository.save(eventoMapper.eventoDTOtoEvento(body));
    }
}
