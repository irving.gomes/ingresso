package com.irving.ingresso.controller;

import com.irving.ingresso.model.Ingresso;
import com.irving.ingresso.model.Usuario;
import com.irving.ingresso.model.map.IngressoMapper;
import com.irving.ingresso.model.map.UsuarioMapper;
import com.irving.ingresso.repository.IngressoRepository;
import com.irving.ingresso.repository.UsuarioRepository;
import io.openapiprocessor.generated.api.UsuarioApi;
import io.openapiprocessor.generated.model.DadosUsuarioDTO;
import io.openapiprocessor.generated.model.IngressoDTO;
import io.openapiprocessor.generated.model.UsuarioDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
@RequiredArgsConstructor
public class UsuarioController implements UsuarioApi {

    private final UsuarioRepository usuarioRepository;
    private final IngressoRepository ingressoRepository;
    private final IngressoMapper ingressoMapper;
    private final UsuarioMapper usuarioMapper;

    private IngressoDTO[] ingressosDTOIterableToArray(Iterable<Ingresso> ingressos) {
        ArrayList<IngressoDTO> ingressosDTOList = new ArrayList<IngressoDTO>();
        ingressos.forEach(ingresso -> {
            ingressosDTOList.add(ingressoMapper.ingressoToIngressoDTO(ingresso));
        });
        IngressoDTO[] toReturn = new IngressoDTO[ingressosDTOList.size()];

        return ingressosDTOList.toArray(toReturn);
    }

    @Override
    public DadosUsuarioDTO listaUsuario(Long id) {
        Usuario usuario = usuarioRepository.findById(id).get();

        DadosUsuarioDTO dadosUsuarioDTO = new DadosUsuarioDTO();
        dadosUsuarioDTO.setUsuario(usuarioMapper.usuarioToUsuarioDTO(usuario));
        dadosUsuarioDTO.setIngressos(ingressosDTOIterableToArray(ingressoRepository.findByUsuario(usuario)));

        return dadosUsuarioDTO;
    }

    @Override
    public void atualizaUsuario(Long id, UsuarioDTO body) {
        if (id == body.getId() && usuarioRepository.findById(id).isPresent()) {
            usuarioRepository.save(usuarioMapper.usuarioDTOtoUsuario(body));
        }
    }

    @Override
    public void deletaUsuario(Long id) {
        usuarioRepository.deleteById(id);
    }

    @Override
    public UsuarioDTO[] listaUsuarios() {
        ArrayList<UsuarioDTO> usuariosDTOList = new ArrayList<UsuarioDTO>();
        usuarioRepository.findAll().forEach(usuario -> {
            usuariosDTOList.add(usuarioMapper.usuarioToUsuarioDTO(usuario));
        });
        UsuarioDTO[] toReturn = new UsuarioDTO[usuariosDTOList.size()];

        return usuariosDTOList.toArray(toReturn);
    }

    @Override
    public void adicionaUsuario(UsuarioDTO body) {
        usuarioRepository.save(usuarioMapper.usuarioDTOtoUsuario(body));
    }
}
