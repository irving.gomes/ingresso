package com.irving.ingresso.controller;

import com.irving.ingresso.model.map.IngressoMapper;
import com.irving.ingresso.repository.IngressoRepository;
import io.openapiprocessor.generated.api.IngressoApi;
import io.openapiprocessor.generated.model.IngressoDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class IngressoController  implements IngressoApi {

    private final IngressoRepository ingressoRepository;
    private final IngressoMapper ingressoMapper;


    @Override
    public void compraIngresso(IngressoDTO body) {
        ingressoRepository.save(ingressoMapper.ingressoDTOtoIngresso(body));
    }
}
