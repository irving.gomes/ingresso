package com.irving.ingresso.repository;

import com.irving.ingresso.model.Ingresso;
import com.irving.ingresso.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface IngressoRepository extends JpaRepository<Ingresso, Long> {
    Iterable<Ingresso> findByUsuario(Usuario usuario);

}