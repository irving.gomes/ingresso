package com.irving.ingresso.repository;

import com.irving.ingresso.model.Evento;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

public interface EventoRepository extends JpaRepository<Evento, Long> {



}